-module(oracle_callback).
-export([handle/2, handle_event/3]).

-include_lib("elli/include/elli.hrl").
-behaviour(elli_handler).

-define(XJSON, {<<"Content-type">>, <<"application/json; charset=ISO-8859-1">>}).

handle(Req, _Args) ->
    %% Delegate to our handler function
    Self=self(),
    Pid=spawn(fun()-> Reply=handle(Req#req.method, elli_request:path(Req), Req), Self ! {reply, Reply} end),
    receive
      {reply, Reply} -> Reply
    after
      1000 -> exit(Pid, kill), {500, [?XJSON], jiffy:encode(#{status=>error,msg=>'Timeout'})}
    end.

handle('POST',[<<"clear">>], _Req) ->
    %% Reply with a normal response. 'ok' can be used instead of '200'
    %% to signal success.
    eredis:q(redis, ["FLUSHDB"]),
    lager:info("Flushing DB:"),
    Reply=jiffy:encode(#{status=>ok, response=>ok}),
    {ok, [?XJSON], Reply};

handle('POST',[<<"sequence">>, <<"create">> | Tail], _Req) when Tail==[] orelse Tail== [<<"test">>] orelse Tail== [<<"timeout">>] ->
    UUID=case Tail of
      [<<"test">>] -> <<"12345678-1234-5678-9abc-123456789abc">>;
      [<<"timeout">>] -> timer:sleep(2000),<<"12345678-1234-5678-9abc-123456789abc">>;
      [] -> uuid:generate()
    end,
    T=term_to_binary({unknown, [], oracle:init1()}),
    {ok, <<"OK">>}=eredis:q(redis, ["SET", UUID, T]),
    {ok,T}=eredis:q(redis, ["GET", UUID]),
    lager:info("CreateSEQ: ~s",[UUID]),
    % store uuid globally
    Reply=jiffy:encode(#{status=>ok, response=> #{sequence=>UUID}}),
    {ok, [?XJSON], Reply};

handle('POST',[<<"observation">>, <<"add">>], Req) ->
    %% Reply with a normal response. 'ok' can be used instead of '200'
    %% to signal success.
    Map=(catch jiffy:decode(Req#req.body, [return_maps])),
    Reply= case Map of
      #{<<"sequence">> :=UUID, <<"observation">> := #{<<"color">> := <<"green">>, <<"numbers">>:= [N1, N2]}}
        when is_binary(N1) andalso size(N1)==7 andalso is_binary(N2) andalso size(N2)==7 ->
        % step of process observation
%        lager:info("N1=~s,N2=~s",[N1,N2]),
        case catch(eredis:q(redis, ["GET", UUID])) of
          {ok, undefined} -> M1= #{status=>error,msg=>'The sequence is\`nt found'};
          {ok, B1} ->
            case catch(binary_to_term(B1)) of
              {red, _, _} -> M1= #{status=>error,msg=>'The red observation should be last'};
              {_, Numbers, Chains} ->
                N1i=binary_to_integer(N1,2),N2i=binary_to_integer(N2,2),
                NewChains=oracle:test2nd({N1i, N2i}, Chains),
                NewNumbers=[{N1i, N2i} | Numbers],
                T=term_to_binary({green, NewNumbers, NewChains}),
                {ok, <<"OK">>}=eredis:q(redis, ["SET", UUID, T]),
                {_, PossibleChains}=lists:unzip(NewChains),
                {P1, P2}=oracle:test3rd(NewChains, NewNumbers),
                P1b=iolist_to_binary(io_lib:format("~7.2.0b",[P1])),
                P2b=iolist_to_binary(io_lib:format("~7.2.0b",[P2])),
                M1= case PossibleChains of
                  [] -> #{status=>error,msg=>'No solutions found'};
                  _ -> #{status=>ok, response=> #{start=> PossibleChains, missing=> [P1b, P2b]}}
                end;
              _ -> M1= #{status=>error,msg=>'b2t error'}
            end;
          _ -> M1= #{status=>error,msg=>'redis GET error'}
        end,
        jiffy:encode(M1);
      #{<<"sequence">> :=UUID, <<"observation">> :=Map2}
        when map_size(Map2)==1 andalso (Map2== #{<<"color">> => <<"red">>}) ->
        case catch(eredis:q(redis, ["GET", UUID])) of
          {ok, undefined} -> M1= #{status=>error,msg=>'The sequence is\`nt found'};
          {ok, B1} ->
            case catch(binary_to_term(B1)) of
              {red, _, _} -> M1= #{status=>error,msg=>'The red observation should be last'};
              {unknown, Numbers, _} ->
                T=term_to_binary({red, Numbers, []}),
                {ok, <<"OK">>}=eredis:q(redis, ["SET", UUID, T]),
                M1= #{status=>error,msg=>'There is\'nt enough data'};
              {green, Numbers, Chains} ->
                case oracle:testred(Chains) of
                  false -> M1= #{status=>error,msg=>'No solutions found'}, [];
                  NewChains ->
                    T=term_to_binary({red, Numbers, NewChains}),
                    {ok, <<"OK">>}=eredis:q(redis, ["SET", UUID, T]),
                    {_, PossibleChains}=lists:unzip(NewChains),
                    {P1, P2}=oracle:test3rd(NewChains, Numbers),
                    P1b=iolist_to_binary(io_lib:format("~7.2.0b",[P1])),
                    P2b=iolist_to_binary(io_lib:format("~7.2.0b",[P2])),
                    M1= #{status=>ok, response=> #{start=> PossibleChains, missing=> [P1b, P2b]}}
                end
            end;
          _ -> M1= #{status=>error,msg=>'redis GET error'}
        end,
        % end of process observation
        jiffy:encode(M1);
      _ -> UUID= "---", jiffy:encode(#{status=>error,msg=>'JSON format error'})
    end,
    lager:info("Reduce(~s): ~s",[UUID, Reply]),
    {ok, [?XJSON], Reply};

handle(_, _, _Req) ->
    {404, [], <<"Not Found">>}.

%% @doc: Handle request events, like request completed, exception
%% thrown, client timeout, etc. Must return 'ok'.
handle_event(_Event, _Data, _Args) ->
    ok.
