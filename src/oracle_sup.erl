-module(oracle_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    {ok, Pid}=supervisor:start_link({local, ?MODULE}, ?MODULE, []),
    [register(redis,Child) || {Id,Child,_,_} <- supervisor:which_children(Pid), Id==redis],
    {ok, Pid}.

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    ElliOpts = [{callback, oracle_callback}, {port, 3000}, {name, {local, elli}}],
    ElliSpec = {elli, {elli, start_link, [ElliOpts]}, permanent, 5000, worker, [elli]},
    RedisSpec = {redis, {eredis, start_link, ["127.0.0.1", 6379, 0, ""]}, permanent, 5000, worker, [eredis]},
    {ok, { {one_for_one, 5, 10}, [ElliSpec, RedisSpec]} }.

