-module(oracle).

-export([start/0, main/0, test1st/1, test2nd/2, test3rd/2, testred/1, init1/0]).

start()->
  application:start(?MODULE).

filter()->
  [
    {0,2#0001000},{1,2#1101101},{2,2#0100010},{3,2#0100100},
    {4,2#1000101},{5,2#0010100},{6,2#0010000},{7,2#0101101},
    {8,2#0000000},{9,2#0000100}
  ].

init2()-> lists:seq(1,99).
init1()-> lists:zip(lists:seq(2,100),lists:seq(1,99)).

%% generate impossible pairs from observation
test1st({V1, V2}) when is_integer(V1) andalso is_integer(V2) ->
  [X1*10+X2 || {X1,Y1}<-filter(),{X2,Y2}<-filter(),(Y1 band V1 /=0) or (Y2 band V2 /=0)];

test1st(_)-> error.

%% reduce incomplete chains with new observation
test2nd({V1, V2}, Chains) when is_integer(V1) andalso is_integer(V2) andalso is_list(Chains) ->
  Possible=init2()-- test1st({V1, V2}),
%  io:format("Possible: ~w~n",[Possible]),
  ReduceChains=fun({K, V})->
    case lists:any(fun(K1)-> K1+1==K end, Possible) of
      false -> [];
      true -> [{K-1, V}]
    end
  end,
  lists:flatmap(ReduceChains, Chains); %% need to optimize

test2nd(_,_)-> error.


%% check broken sections
test3rd(Chains, Numbers) ->
  F=fun(Chain)->
    FF=fun({P1in, P2in},{{P1sum,P2sum},{Nmin,Nmax}})->
      D1=Nmax div 10,
      D2=Nmax rem 10,
      {D1, P1}= lists:nth(D1+1,filter()),
      {D2, P2}= lists:nth(D2+1,filter()),
%      io:format("~b ~b ~7.2.0b ~7.2.0b ~7.2.0b ~7.2.0b ~7.2.0b ~7.2.0b~n",[D1, D2, P1in, P2in, P1, P2, P1sum, P2sum]),
      {{P1sum bor (bnot P1 bxor P1in) band 2#1111111,P2sum bor (bnot P2 bxor P2in) band 2#1111111},{Nmin,Nmax-1}}
    end,
%    io:format(">"),
    {Pattern, _}=lists:foldr(FF, {{2#0000000, 2#0000000}, Chain}, Numbers),
    Pattern
  end,
  F2=fun({X1,X2},{Y1,Y2})->
    {X1 band Y1, X2 band Y2}
  end,
  lists:foldl(F2,{2#1111111,2#1111111},lists:map(F,Chains)).

testred([{1,X}|_]) -> [{1,X}];

testred(_) -> false.

main()->
%  Z=[{2#0100100,2#1000010},{2#1110111,2#1110111},{2#0100100,2#1010010},{2#0000000,2#1101100}],
%  Z=lists:reverse([{2#0100100,2#1000010},{2#1110111,2#1110111},{2#0101100,2#1010010}]),
%  Z=[{2#0100100,2#1000010},{2#1110111,2#1110111}],
%  Z=[{2#0100100,2#1000010}],
  Z=lists:reverse([{2#1110111,2#0011101},{2#1110111,2#0010000}]),
  [io:format("~7.2.0b ~7.2.0b,",[X1,X2])||{X1,X2}<-Z],
  ZZ=lists:foldr(fun test2nd/2,init1(),Z),
  io:format("~nResult: ~p~n",[ZZ]),
  {P1,P2}=test3rd(ZZ,Z),
%  [io:format("~7.2.0b ~7.2.0b,",[X1,X2])||{X1,X2}<-L].
  io:format("~7.2.0b ~7.2.0b~n",[P1,P2]).

% lists:foldl(fun prog:test2nd/2,prog:init1(),[{2#0100100,2#1000010}]).
% lists:foldl(fun prog:test2nd/2,prog:init1(),[{2#0100100,2#1000010},{2#0100100,2#1000010}]).
% Z=[{2#0100100,2#1000010},{2#1110111,2#1110111},{2#0100100,2#1010010},{2#0000000,2#1101100}].
% ZZ=lists:foldl(fun prog:test2nd/2,prog:init(),Z).
