FROM ubuntu:trusty
COPY rel/oracle /oracle
RUN apt-get update && apt-get install -y redis-server
CMD service redis-server start && oracle/bin/oracle console
EXPOSE 3000
