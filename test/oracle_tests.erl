-module(oracle_tests).

-include_lib("eunit/include/eunit.hrl").

oracle_test()->
  Z=lists:reverse([{2#1110111,2#0011101},{2#1110111,2#0010000}]),
  ZZ=lists:foldr(fun oracle:test2nd/2,oracle:init1(),Z),
  {2#0000000,2#1000010}=oracle:test3rd(ZZ,Z),
  {_,[2,8,82,88]}=lists:unzip(ZZ),
  [{_,2}]=oracle:testred(ZZ).

oracle2_test()->
  Z=lists:reverse([{2#1111111,2#1111111},{2#1111111,2#1111111}]),
  []=lists:foldr(fun oracle:test2nd/2,oracle:init1(),Z).

oracle_test_()->
  [
    ?_assert(oracle:testred([{1,99},tail])==[{1,99}]),
    ?_assert(oracle:testred([{2,98},{3,99}])==false),
    ?_assert(oracle:test1st(0)==error),
    ?_assert(oracle:test2nd(0,true)==error)
  ].
