#!/bin/sh
SEQ='test1'
Q1='{"observation":{"color":"green","numbers":["1110111","0011101"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q1E='{"observation":{"color":"yellow","numbers":"1110111"},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q2='{"observation":{"color":"green","numbers":["1110111","0010000"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q2E='{"observation":{"color":"green","numbers":["111011","0010000"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q3E='{"observation":{"color":"red","numbers":["1110111","0011101"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q3='{"observation":{"color":"red"},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q8='{"observation":{"color":"green","numbers":["1111111","1111111"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q8E='{"observation":{"color":"green","numbers":["1111111"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q87='{"observation":{"color":"green","numbers":["1101111", "1010000"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q86='{"observation":{"color":"green","numbers":["1101111", "1101101"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q85='{"observation":{"color":"green","numbers":["1101111", "1101001"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
curl http://localhost:3000/clear -d ""
echo .
echo Testing with no sequence...
curl http://localhost:3000/observation/add -d "$Q3"
echo .
curl http://localhost:3000/observation/add -d "$Q1"
echo .
curl http://localhost:3000/observation/add -d "$Q1E"
echo .
curl http://localhost:3000/observation/add -d "$Q2E"
echo .
echo Creating 2 sequences
curl http://localhost:3000/sequence/create -d ""
echo .
curl http://localhost:3000/sequence/create/test -d ""
echo .
echo Waiting timeout...
curl http://localhost:3000/sequence/create/timeout -d ""
echo .
echo Testing red only.
curl http://localhost:3000/observation/add -d "$Q3"
echo .
echo Testing 88 after 88...
curl http://localhost:3000/sequence/create/test -d ""
echo .
curl http://localhost:3000/observation/add -d "$Q8E"
echo .
curl http://localhost:3000/observation/add -d "$Q8"
echo .
curl http://localhost:3000/observation/add -d "$Q8"
echo .
echo Testing red after 88...
curl http://localhost:3000/sequence/create/test -d ""
echo .
curl http://localhost:3000/observation/add -d "$Q8"
echo .
curl http://localhost:3000/observation/add -d "$Q3"
echo .
echo Testing normal sequence with red in end...
curl http://localhost:3000/sequence/create/test -d ""
echo .
curl http://localhost:3000/observation/add -d "$Q1"
echo .
curl http://localhost:3000/observation/add -d "$Q2"
echo .
curl http://localhost:3000/observation/add -d "$Q3E"
echo .
curl http://localhost:3000/observation/add -d "$Q3"
echo .
echo Testing green after red...
curl http://localhost:3000/observation/add -d "$Q1"
echo .
echo Testing 87...
curl http://localhost:3000/sequence/create/test -d ""
echo .
curl http://localhost:3000/observation/add -d "$Q87"
echo .
curl http://localhost:3000/observation/add -d "$Q86"
echo .
curl http://localhost:3000/observation/add -d "$Q85"
