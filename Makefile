APP := oracle
REBAR = rebar
.PHONY: deps

all: deps
	@$(REBAR) compile

deps:
	@$(REBAR) get-deps

clean:
	@$(REBAR) skip_deps=true clean

eunit:
	@$(REBAR) skip_deps=true eunit

distclean: clean
	@$(REBAR) delete-deps

docs:
	@$(REBAR) skip_deps=true doc

run:
	@erl -pa ebin deps/*/ebin -s $(APP)

cloc:
	cloc src test

release:
	rebar generate -f

docker:
	docker build -t oracle .

daemon:
	docker run -dt -p 3000:3000 --name oracle oracle
