#!/bin/sh
SEQ='test1'
Q29='{"observation":{"color":"green","numbers":["1011101","1111011"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q28='{"observation":{"color":"green","numbers":["1011101","1111111"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q27='{"observation":{"color":"green","numbers":["1011101","1010010"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q26='{"observation":{"color":"green","numbers":["1011101","1101111"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q25='{"observation":{"color":"green","numbers":["1011101","1101011"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q24='{"observation":{"color":"green","numbers":["1011101","0111010"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q23='{"observation":{"color":"green","numbers":["1011101","1011011"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q22='{"observation":{"color":"green","numbers":["1011101","1011101"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q21='{"observation":{"color":"green","numbers":["1011101","0010010"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
Q20='{"observation":{"color":"green","numbers":["1011101","1110111"]},"sequence":"12345678-1234-5678-9abc-123456789abc"}'
curl http://localhost:3000/clear -d ""
echo .
curl http://localhost:3000/sequence/create/test -d ""
echo .
echo Testing 27, 26...
curl http://localhost:3000/observation/add -d "$Q29"
echo .
curl http://localhost:3000/observation/add -d "$Q28"
echo .
curl http://localhost:3000/observation/add -d "$Q27"
echo .
curl http://localhost:3000/observation/add -d "$Q26"
echo .
curl http://localhost:3000/observation/add -d "$Q25"
echo .
curl http://localhost:3000/observation/add -d "$Q24"
echo .
curl http://localhost:3000/observation/add -d "$Q23"
echo .
curl http://localhost:3000/observation/add -d "$Q22"
echo .
curl http://localhost:3000/observation/add -d "$Q21"
echo .
curl http://localhost:3000/observation/add -d "$Q20"
echo .
